package pjct.password.manager;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Save {	

	public static void save_password(List<Password> pwd) {
		File file = new File("current_password.ser");
		ObjectOutputStream stream = null;
		try {
			stream = new ObjectOutputStream(new FileOutputStream(file));
			stream.writeObject(pwd);
			stream.close();
		}catch(IOException ioe) {
			System.out.println("Can't save password");
		}
	}

	public static List<Password> load_password(){
		ObjectInputStream stream = null;
		try {
			stream = new ObjectInputStream(new FileInputStream("current_password.ser"));
			List<Password> loading = (List<Password>)stream.readObject();
			stream.close();
			//System.out.println(loading.toString());
			return loading;
		}catch(Exception e) {
			System.out.println("Error in loading password file");
			e.toString();
		}
		return new ArrayList<Password>();
	}
	

}