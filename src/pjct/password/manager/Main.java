package pjct.password.manager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Main{

	
	public static void main(String[] args) {
		List<Password> passwords = Save.load_password();
		Password pwd = new Password(5, "Wow");
		//System.out.println(pwd.toString());
		passwords.add(pwd);
		Save.save_password(passwords);
		
		passwords = Save.load_password();
		for (Password password : passwords) {
			System.out.println(password.toString());
		}
		
	}

}