package pjct.password.manager;

import java.time.LocalDate;

import java.util.Objects;
import java.util.Random;


import java.io.Serializable;

public class Password implements Serializable{

	private final static int MINIMUM_VALUE = Integer.parseInt("21", 16); 
	private final static int MAXIMUM_VALUE = Integer.parseInt("7e", 16);
	
	private int size;
	private String key;
	private String value;
	
	public Password(String key) {
		this.size = 15;
		this.key = Objects.requireNonNullElse(key, "password generated at " + LocalDate.now());
		generator(key);
	}
	
	public Password(int size, String key) {
		if(size <= 0) {
			System.out.println("Not a valid size\n");
			System.exit(-1);
		}else {
			this.size = size;
			this.key = Objects.requireNonNullElse(key, "password generated at " + LocalDate.now());
			generator(key);
		}
	}
	
	public void generator(String key) {
		Random rnd = new Random();
		this.value = "";
		int val = 0;
		for(int i = 0 ; i < this.size ; i++) {
			val = rnd.nextInt(MAXIMUM_VALUE + 1);
			if (val < MINIMUM_VALUE) {
				val = val + MINIMUM_VALUE;
			}
			//System.out.println("integer value : " + val + "\t hexa value : " + Integer.toHexString(val));
			this.value = this.value + ((char)val);
		}
	}

	public static int getMinimumValue() {
		return MINIMUM_VALUE;
	}

	public static int getMaximumValue() {
		return MAXIMUM_VALUE;
	}
	
	public String toString() {
		return "key : " + this.key + "\t\tvalue : " + this.value +"\n++++++++++";
	}

}